from setuptools import setup
from application import __version__

setup(name='Jupebox',
      version=__version__,
      author='Ricky',
      packages=['application']
      )

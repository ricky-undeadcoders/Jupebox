FROM rickyundeadcoders/alpine-py3:3.6.3
MAINTAINER Ricky Whitaker

# set up alpine env
ENV LIBRARY_PATH=/lib:/usr/lib
RUN apk update; apk add build-base jpeg-dev zlib-dev libffi-dev postfix tzdata; \
    cp /usr/share/zoneinfo/US/Mountain /etc/localtime

# set up python venv
COPY Pipfile /Pipfile
COPY Pipfile.lock /Pipfile.lock
WORKDIR /
RUN pip3 install pipenv --upgrade pip; pipenv install --system;

# set up app directory and pull in application files
RUN mkdir /application;
COPY /application/ /application/
WORKDIR /application

# turn our application into a packages so we can import it around
COPY setup.py /setup.py
RUN pip3 install -e /

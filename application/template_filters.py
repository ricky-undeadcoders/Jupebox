from application.middleware import (split_datetime, pretty_date, pretty_time, clean_javascript_text,
                                    format_error_message, slugify,
                                    jsonify, remove_whitespace, pretty_date_and_time)

template_filters = [split_datetime, pretty_date, pretty_time, clean_javascript_text, format_error_message, slugify,
                    jsonify, remove_whitespace, pretty_date_and_time]

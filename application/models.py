#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask_security import UserMixin, RoleMixin, AnonymousUser
from flask_security.utils import string_types
from sqlalchemy import (UniqueConstraint, Column, Integer, ForeignKey, String, Boolean, DateTime,
                        Float, TEXT, func)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from datetime import datetime

from application.config import datastore_config as ds_config

Base = declarative_base()
metadata = Base.metadata


class CustomAnonymousUser(AnonymousUser):
    def __init__(self):
        AnonymousUser.__init__(self)

    @property
    def is_admin_user(self):
        return False


'''
#####################################################################
# BASE FUNCTIONS
#####################################################################
'''


class BaseUser(Base, UserMixin):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    password = Column(String(length=255), nullable=False)
    username = Column(String(length=ds_config.USER_USERNAME_MAX_LENGTH))
    first_name = Column(String(length=ds_config.USER_FIRST_NAME_MAX_LENGTH))
    last_name = Column(String(length=ds_config.USER_LAST_NAME_MAX_LENGTH))
    email = Column(String(length=ds_config.USER_EMAIL_MAX_LENGTH), unique=True, nullable=False)

    role_id = Column(ForeignKey('role.id'))
    role = relationship('Role', foreign_keys=[role_id])
    messages = relationship('Message', secondary='user_messages')
    message_groups = relationship('MessageGroup', secondary='user_message_groups')

    last_login_at = Column(DateTime())
    current_login_at = Column(DateTime())
    last_login_ip = Column(String(length=25))
    current_login_ip = Column(String(length=25))
    login_count = Column(Integer(), default=0)
    confirmed_at = Column(DateTime())
    active = Column(Boolean(), default=True)
    create_date = Column(DateTime(), default=datetime.now)  # TODO: get this reporting central time
    updated_date = Column(DateTime(), onupdate=datetime.now)

    @property
    def is_admin_user(self):
        if self.role.name == 'admin':
            return True
        return False

    @property
    def is_active(self):
        """Returns `True` if the user is active."""
        return self.active

    def has_role(self, role):
        """Returns `True` if the user identifies with the specified role.

        :param role: A role name or `Role` instance"""
        if isinstance(role, string_types):
            return role == self.role.name
        else:
            return role == self.role


class Role(Base, RoleMixin):
    __tablename__ = "role"

    id = Column(Integer, primary_key=True)
    name = Column(String(length=ds_config.ROLE_NAME_MAX_LENGTH), unique=True, nullable=False)
    description = Column(String(length=ds_config.ROLE_DESCRIPTION_MAX_LENGTH))

    users = relationship('User', back_populates='role')

    active = Column(Boolean(), default=True)
    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


class FAQ(Base):
    __tablename__ = 'faq'

    id = Column(Integer, primary_key=True, autoincrement=True)
    order = Column(Integer, nullable=False)
    question = Column(String(length=ds_config.FAQ_QUESTION_MAX_LENGTH), nullable=False)
    answer = Column(String(length=ds_config.FAQ_ANSWER_MAX_LENGTH), nullable=False)

    active = Column(Boolean(), default=False)
    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


class Contact(Base):
    __tablename__ = 'contact'

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(length=250))
    name = Column(String(length=250))
    message = Column(String(length=2048))


###################################################################################
# MESSAGE MODELS
###################################################################################

class MessageType(Base):
    __tablename__ = 'message_type'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(length=250), unique=True, nullable=False)
    description = Column(String(length=500), nullable=False)

    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


class MessageGroup(Base):
    __tablename__ = 'message_group'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(length=250), unique=True, nullable=False)
    description = Column(String(length=1024), nullable=False)

    users = relationship('User', secondary='user_message_groups')

    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


class Message(Base):
    __tablename__ = 'message'

    id = Column(Integer, primary_key=True, autoincrement=True)
    subject = Column(String(length=ds_config.MESSAGE_HEADER_MAX_LENGTH), nullable=False)
    body = Column(String(length=ds_config.MESSAGE_BODY_MAX_LENGTH), nullable=False)

    type_id = Column(ForeignKey('message_type.id'), nullable=False)
    type = relationship('MessageType')
    users = relationship('User', secondary='user_messages')


class UserMessages(Base):
    __tablename__ = 'user_messages'

    id = Column(Integer, primary_key=True, autoincrement=True)
    message_id = Column(Integer, ForeignKey('message.id'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    read_date = Column(DateTime())
    deleted_date = Column(DateTime())
    create_date = Column(DateTime(), default=datetime.now)
    user = relationship('User', backref='message')
    message = relationship('Message', backref='user')


class UserMessageGroups(Base):
    __tablename__ = 'user_message_groups'

    id = Column(Integer, primary_key=True, autoincrement=True)
    message_group_id = Column(Integer, ForeignKey('message_group.id'), nullable=False)
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


###################################################################################
# AUDIT MODELS
###################################################################################

class Audit(Base):
    __tablename__ = 'audit'

    id = Column(Integer, primary_key=True, autoincrement=True)

    audit_action_id = Column(ForeignKey('audit_action.id'))
    audit_action = relationship('AuditAction')
    action_date = Column(DateTime())  # this is the actual action date, need to set this at the signals level

    user_id = Column(ForeignKey('user.id'))
    user = relationship('User', foreign_keys=[user_id])
    admin_user_id = Column(ForeignKey('user.id'))
    admin_user = relationship('User', foreign_keys=[admin_user_id])

    create_date = Column(DateTime(), default=datetime.now)
    # I'm leaving this in because I'm fascinated to see what the delay will be, not needed for future projects


class AuditAction(Base):
    __tablename__ = 'audit_action'

    id = Column(Integer, primary_key=True, autoincrement=True)

    name = Column(String(length=50), unique=True, nullable=False)
    description = Column(String(length=2400))

    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


class Config(Base):
    __tablename__ = 'config'

    id = Column(Integer, primary_key=True, autoincrement=True)

    name = Column(String(length=100), unique=True, nullable=False)
    description = Column(String(length=2400))
    value = Column(String(length=2400))

    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)

'''
#####################################################################
# /BASE FUNCTIONS
#####################################################################
'''

'''
#####################################################################
# CONTENT FUNCTIONS
#####################################################################
'''
from application.config.content import datastore_config as content_ds_config


class Content(Base):
    __tablename__ = 'content'

    id = Column(Integer, primary_key=True, autoincrement=True)
    text = Column(String(length=content_ds_config.CONTENT_TEXT_MAX_LENGTH))
    order = Column(Integer)
    location = Column(String(length=100))

    type_id = Column(Integer, ForeignKey('content_type.id'))
    type = relationship('ContentType', foreign_keys=[type_id])

    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


class ContentDraft(Base):
    __tablename__ = 'content_draft'

    id = Column(Integer, primary_key=True, autoincrement=True)
    text = Column(String(length=content_ds_config.CONTENT_TEXT_MAX_LENGTH))
    order = Column(Integer)

    type_id = Column(Integer, ForeignKey('content_type.id'))
    type = relationship('ContentType', foreign_keys=[type_id])

    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


class ContentType(Base):
    __tablename__ = 'content_type'
    __table_args__ = (UniqueConstraint('name', 'page', name='_name_page_uc'),)

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(length=25))
    page = Column(String(length=50))

    create_date = Column(DateTime(), default=datetime.now)
    updated_date = Column(DateTime(), onupdate=datetime.now)


'''
#####################################################################
# /CONTENT FUNCTIONS
#####################################################################
'''

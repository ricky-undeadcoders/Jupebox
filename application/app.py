#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import Flask, g, abort, render_template, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_scss import Scss
from glob import glob

from logging.config import dictConfig
from datetime import datetime as DT

from application.models import (CustomAnonymousUser)

from application.blueprints import blueprints
from application.config import base_config, datastore_config, verbiage_config
from application.datastore import BaseDataStore
from application.extensions import security, mail, redis_store
from application import __version__
from application.template_filters import template_filters
from application.signals import register_audit_trails


#################################################
# BUILD OUR FLASK APPLICATION
#################################################

def create_app(configs=None):
    if configs is None:
        filename = '/var/log/app/{}-app.log'.format(DT.now().strftime('%Y%m%d'))

        dictConfig({
            'version': 1,
            'formatters': {'default': {
                'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
            }},
            'handlers': {'wsgi': {
                'class': 'logging.FileHandler',
                'filename': filename,
                'formatter': 'default'
            }},
            'root': {
                'level': 'INFO',
                'handlers': ['wsgi']
            }
        })
    app = Flask(import_name=__name__)
    app.config.from_object(base_config)
    app.config.from_object(datastore_config)
    app.config.from_object(verbiage_config)

    if configs:
        for config in configs:
            app.config.from_object(config)
    else:
        app.logger.info('No configs passed in, loading from /home/config')
        for filename in glob('/config/*config.py'):
            app.logger.info('Loading: {}'.format(filename))
            app.config.from_pyfile(filename)

    app.logger.info('Loaded app with file configs, launching datastore')
    db = SQLAlchemy(app)
    user_datastore = BaseDataStore(db=db)

    app.logger.info('Launching security extension')
    security.init_app(app=app,
                      datastore=user_datastore,
                      anonymous_user=CustomAnonymousUser,
                      register_blueprint=False)
    app.logger.info('Launching all other extensions')
    mail.init_app(app)
    redis_store.init_app(app)

    app.logger.info('Adding blueprints')
    for blueprint in blueprints:
        app.logger.debug(f'Adding blueprint: {blueprint.name}')
        app.register_blueprint(blueprint)

    app.logger.info('Instantiating Scss object')
    Scss(app)

    app.logger.info('Registering audit trails')
    register_audit_trails()

    app.logger.info('Now adding error handling and before_request commands')

    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('404.html'), 404

    @app.before_request
    def before_request():
        try:
            # set up easier reference to our datastore, also error check that it exists
            g.datastore = app.extensions['security'].datastore
            g.redis_store = app.extensions['redis']
        except Exception as e:
            app.logger.critical(f'Failed trying to set up before_request references: {e}')
            abort(500)

    @app.after_request
    def add_header(response):
        """
        Add headers to both force latest IE rendering engine or Chrome Frame,
        and also to cache the rendered page for 10 minutes.
        """
        response.headers['X-UA-Compatible'] = 'IE=Edge,chrome=1'
        response.headers['Cache-Control'] = 'public, max-age=0'
        return response

    app.logger.info('Aaaaaaaannnnndddddd basic views.....')

    @app.route('/version/', methods=['GET'])
    def version():
        return __version__

    @app.route('/site-map/', methods=['GET'])
    def site_map():
        view_functions = []
        for url_rule, url_function in app.view_functions.items():
            try:
                bp, view = url_rule.split('.')
                if bp in app.config['SITE_MAP_BLUEPRINTS'] and 'draft' not in view:
                    view_functions.append((url_rule, url_for(url_rule)))
            except:
                pass
        view_functions.sort()
        return render_template('main/site_map.html',
                               view_functions=view_functions)

    app.logger.debug('And template filters......')
    for template_filter in template_filters:
        app.add_template_filter(template_filter)

    return app


def create_celery_app(app=None):
    """
    Create a new Celery object and tie together the Celery config to the app's
    config. Wrap all tasks in the context of the application.
    :param app: Flask app
    :return: Celery app
    """
    from celery import Celery

    app = app or create_app()

    CELERY_TASK_LIST = [
        'application.tasks',
    ]

    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'],
                    include=CELERY_TASK_LIST)
    celery.conf.update(app.config)
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery

#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import g
from flask_security.signals import (signals,
                                    user_registered,
                                    user_confirmed,
                                    confirm_instructions_sent,
                                    login_instructions_sent,
                                    password_reset,
                                    password_changed,
                                    reset_password_instructions_sent)

user_registered_by_admin = signals.signal("user-registered-by-admin")


def password_changed_audit(sender, **kwargs):
    """
    available kwargs: user
    """
    g.datastore.create_audit(audit_action_name='password_changed',
                             user=kwargs['user'])


def password_reset_audit(sender, **kwargs):
    """
    available kwargs: user
    """
    g.datastore.create_audit(audit_action_name='password_reset',
                             user=kwargs['user'])


def user_registered_audit(sender, **kwargs):
    """
    available kwargs: user, token
    """
    g.datastore.create_role_message(subject='User Registered',
                                    body='{} has registered for the site.'.format(kwargs['user'].email),
                                    message_type='Notification',
                                    role='admin')
    g.datastore.create_audit(audit_action_name='user_registered',
                             user=kwargs['user'])


def user_registered_by_admin_audit(sender, **kwargs):
    """
    available kwargs: user, token
    """
    g.datastore.create_audit(audit_action_name='user_registered_by_admin',
                             user=kwargs['user'],
                             admin_user=kwargs['admin_user'])


def register_audit_trails():
    password_changed.connect(password_changed_audit, weak=False)
    password_reset.connect(password_reset_audit, weak=False)
    user_registered.connect(user_registered_audit, weak=False)
    user_registered_by_admin.connect(user_registered_by_admin_audit, weak=False)


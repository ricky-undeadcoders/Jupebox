#!/usr/bin/python3
# -*- coding: utf-8 -*-
TESTING = True
DEBUG = True

SECRET_KEY = 'QASTuipdSECRETK3yTh@TmIghtSti1lb33t0ught00hack'
SECURITY_PASSWORD_SALT = 'QASTuipdSECRETK3yTh@TmIghtSti1lb33t0yght00hack'

SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:abc123@localhost/'

MAIL_SERVER = 'smtp.mailgun.org'
MAIL_PORT = 2525
MAIL_USE_SSL = False
MAIL_USERNAME = ''
MAIL_PASSWORD = ''

REDIS_URL = "redis://localhost:6379/0"

USE_GOOGLE_ANALYTICS = False

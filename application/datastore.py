# !/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import current_app
from flask_security.utils import get_identity_attributes, string_types
from sqlalchemy import func, text, or_, all_, desc, asc, column

from application.models import *


class BaseDataStore(object):
    def __init__(self, db):
        self.db = db

    '''
    #####################################################################
    # BASE FUNCTIONS
    #####################################################################
    '''

    def commit(self):
        self.db.session.flush()
        self.db.session.commit()

    def put(self, model):
        self.db.session.add(model)
        self.commit()

    def delete(self, model):
        self.db.session.delete(model)
        self.commit()

    def search(self, q, model):
        if not q:
            return ''

        search_query = '%{0}%'.format(q)
        # search_chain = []

        # for this_field in search_fields:
        #     this_search = model.this_field.ilike(search_query)
        #     search_chain.append(this_search)
        search_chain = [model.email.ilike(search_query),
                        model.username.ilike(search_query),
                        model.first_name.ilike(search_query),
                        model.last_name.ilike(search_query)]

        return or_(*search_chain)

    def _is_numeric(self, value):
        try:
            int(value)
        except (TypeError, ValueError):
            return False
        return True

    def deactivate_model(self, model):
        """Deactivates a specified user. Returns `True` if a change was made.

        :param user: The user to deactivate
        """
        try:
            if model.active:
                model.active = False
                return True
            return False
        except:
            return False

    def activate_model(self, model):
        """Activates a specified user. Returns `True` if a change was made.

        :param user: The user to activate
        """
        try:
            if not model.active:
                model.active = True
                return True
            return False
        except:
            return False

    #####################################################################
    # USER AND ADMIN USER FUNCTIONS
    #####################################################################

    def get_user(self, identifier):
        if self._is_numeric(identifier):
            return self.db.session.query(User).get(identifier)
        for attr in get_identity_attributes():
            query = func.lower(getattr(User, attr)) \
                    == func.lower(identifier)
            rv = self.db.session.query(User).filter(query).first()
            if rv is not None:
                return rv

    def find_user(self, **kwargs):
        user = self.db.session.query(User).filter_by(**kwargs).first()
        return user

    def create_user(self, **kwargs):
        """Creates and returns a new user from the given parameters."""
        if kwargs.get('role'):
            role = self.find_role(name=kwargs.pop('role'))
        else:
            role = self.find_role(id=1)
        user = User(**kwargs)
        user.role = role
        self.put(user)
        return user

    def find_users(self, role=None, **kwargs):
        if role:
            role = self._prepare_role_arg(role)
            users = self.db.session.query(User).filter_by(**kwargs).all()
            users_with_role = []
            for user in users:
                if role == user.role:
                    users_with_role.append(user)
            return users_with_role
        return self.db.session.query(User).filter_by(**kwargs).all()

    def modify_user(self, user, **kwargs):
        if kwargs.get('username'):
            user.username = kwargs['username']
        if kwargs.get('first_name'):
            user.first_name = kwargs['first_name']
        if kwargs.get('last_name'):
            user.last_name = kwargs['last_name']
        if kwargs.get('first_name'):
            user.first_name = kwargs['first_name']
        if kwargs.get('email'):
            user.email = kwargs['email']
        if kwargs.get('role'):
            user.role = kwargs['role']
        if kwargs.get('active') is not None:
            user.active = kwargs['active']
        self.put(user)
        return user

    #####################################################################
    # ROLE AND ADMIN ROLE FUNCTIONS
    #####################################################################

    def _prepare_role_modify_args(self, user, role):
        if isinstance(user, string_types):
            user = self.find_user(email=user)
        if isinstance(role, string_types):
            role = self.find_role(name=role)
        return user, role

    def _prepare_role_arg(self, role):
        if isinstance(role, string_types):
            role = self.find_role(name=role)
        return role

    def create_role(self, **kwargs):
        """Creates and returns a new role from the given parameters."""
        role = Role(**kwargs)
        self.put(role)
        return role

    def find_role(self, **kwargs):
        return self.db.session.query(Role).filter_by(**kwargs).first()

    def find_roles(self, **kwargs):
        # return self.db.session.query(Role).all()
        return self.db.session.query(Role).filter_by(**kwargs).all()

    def modify_role(self, role, **kwargs):
        if kwargs.get('name'):
            role.name = kwargs['name']
        if kwargs.get('description'):
            role.description = kwargs['description']
        self.put(role)
        return role

    #####################################################################
    # FAQ FUNCTIONS
    #####################################################################

    def create_faq(self, **kwargs):
        """Creates and returns a new role from the given parameters."""
        faq = FAQ(**kwargs)
        self.put(faq)
        return faq

    def modify_faq(self, faq, **kwargs):
        if kwargs.get('order'):
            faq.order = kwargs['order']
        if kwargs.get('question'):
            faq.question = kwargs['question']
        if kwargs.get('answer'):
            faq.answer = kwargs['answer']
        self.put(faq)
        return faq

    def find_faq(self, **kwargs):
        return self.db.session.query(FAQ).filter_by(**kwargs).first()

    def find_faqs(self, order_by=None, **kwargs):
        if order_by:
            try:
                return self.db.session.query(FAQ).order_by(order_by).all()
            except:
                pass
        return self.db.session.query(FAQ).order_by('order').all()

    #####################################################################
    # CONTACT FUNCTIONS
    #####################################################################

    def create_contact(self, **kwargs):
        """Creates and returns a new role from the given parameters."""
        contact = Contact(**kwargs)
        self.put(contact)
        return contact

    def find_contact(self, **kwargs):
        return self.db.session.query(Contact).filter_by(**kwargs).first()

    def find_contacts(self, order_by=None, **kwargs):
        if order_by:
            try:
                return self.db.session.query(Contact).filter_by(**kwargs).order_by(order_by).all()
            except:
                pass
        return self.db.session.query(Contact).filter_by(**kwargs).order_by('id asc').all()

    #####################################################################
    # MESSAGE FUNCTIONS
    #####################################################################

    def create_message(self, **kwargs):
        """Creates and returns a new role from the given parameters."""
        message = Message(**kwargs)
        self.put(message)
        return message

    def modify_message(self, message, **kwargs):
        if kwargs.get('subject'):
            message.subject = kwargs['subject']
        if kwargs.get('body'):
            message.body = kwargs['body']
        if kwargs.get('type'):
            message.type = kwargs['type']
        self.put(message)
        return message

    def find_message(self, **kwargs):
        return self.db.session.query(Message).filter_by(**kwargs).first()

    def find_messages(self, order_by=None, **kwargs):
        if order_by:
            try:
                return self.db.session.query(Message).order_by(order_by).all()
            except:
                pass
        return self.db.session.query(Message).all()

    def create_user_messages(self, subject, body, message_type, users):
        message_type = self.find_message_type(message_type)
        message = self.create_message(subject=subject, body=body, type=message_type, users=users)
        return message

    def create_group_message(self, subject, body, message_type, message_group):
        if isinstance(message_group, string_types):
            users = self.find_message_group(name=message_group).users
        elif isinstance(message_group, MessageGroup):
            users = message_group.users
        else:
            users = self.find_message_group(id=message_group).users
        return self.create_user_messages(subject, body, message_type, users)

    def create_role_message(self, subject, body, message_type, role):
        if isinstance(role, string_types):
            users = self.find_role(name=role).users
        elif isinstance(role, Role):
            users = role.users
        else:
            users = self.find_role(id=role).users
        return self.create_user_messages(subject, body, message_type, users)

    def create_message_type(self, **kwargs):
        """Creates and returns a new role from the given parameters."""
        message_type = MessageType(**kwargs)
        self.put(message_type)
        return message_type

    def find_message_type(self, message_type):
        if isinstance(message_type, string_types):
            return_message_type = self.db.session.query(MessageType).filter_by(name=message_type).first()
            if return_message_type is None:
                return_message_type = self.create_message_type(name=message_type, description='Auto Generated')
            return return_message_type
        else:
            return None

    def create_message_group(self, **kwargs):
        """Creates and returns a new role from the given parameters."""
        message_group = MessageGroup(**kwargs)
        self.put(message_group)
        return message_group

    def modify_message_group(self, message_group, **kwargs):
        if kwargs.get('name'):
            message_group.name = kwargs['name']
        if kwargs.get('description'):
            message_group.description = kwargs['description']
        if kwargs.get('users'):
            message_group.users = kwargs['users']
        self.put(message_group)
        return message_group

    def find_message_group(self, **kwargs):
        return self.db.session.query(MessageGroup).filter_by(**kwargs).first()

    def find_message_groups(self, order_by=None, **kwargs):
        if order_by:
            try:
                return self.db.session.query(MessageGroup).order_by(order_by).all()
            except:
                pass
        return self.db.session.query(MessageGroup).all()

    def find_user_message(self, **kwargs):
        msgs = self.db.session.query(UserMessages).filter_by(**kwargs)
        return msgs.first()

    def find_user_messages(self, user):
        msgs = self.db.session.query(UserMessages).filter_by(user_id=user.id)
        print(msgs)
        msgs = msgs.filter(column('deleted_date').is_(None))
        print(msgs)
        return msgs.all()

    def find_unread_messages(self, user):
        msgs = self.db.session.query(UserMessages).filter_by(user_id=user.id)
        msgs = msgs.filter(column('read_date').is_(None))
        msgs = msgs.filter(column('deleted_date').is_(None))
        return msgs.all()

    def modify_user_message(self, user_message, **kwargs):
        if kwargs.get('read_date'):
            if isinstance(kwargs['read_date'], datetime):
                user_message.read_date = kwargs['read_date']
            else:
                if kwargs['read_date'].lower() == 'delete':
                    user_message.read_date = None
        if kwargs.get('deleted_date'):
            user_message.deleted_date = kwargs['deleted_date']
        self.put(user_message)
        return user_message

    #############################################################
    # AUDIT FUNCTIONS
    #############################################################

    def create_audit(self, audit_action_name, **kwargs):
        """Creates and returns a new blog post from the given parameters."""
        audit_action = self.find_audit_action(name=audit_action_name)
        if audit_action is None:
            audit_action = self.create_audit_action(name=audit_action_name,
                                                    description='This audit action was auto generated')
        audit = Audit(**kwargs)
        audit.audit_action = audit_action
        self.put(audit)
        return audit

    def create_audit_action(self, **kwargs):
        aa = AuditAction(**kwargs)
        self.put(aa)
        return aa

    def find_audit(self, **kwargs):
        return self.db.session.query(Audit).filter_by(**kwargs).first()

    def find_audits(self, order_by=None, **kwargs):
        if order_by:
            return self.db.session.query(Audit).filter_by(**kwargs).order_by(order_by).all()
        return self.db.session.query(Audit).filter_by(**kwargs).order_by('id').all()

    def find_audit_action(self, **kwargs):
        return self.db.session.query(AuditAction).filter_by(**kwargs).first()

    def find_audit_actions(self, order_by=None, **kwargs):
        if order_by:
            return self.db.session.query(AuditAction).filter_by(**kwargs).order_by(order_by).all()
        return self.db.session.query(AuditAction).filter_by(**kwargs).order_by(desc('id')).all()

    #############################################################
    # CONFIG FUNCTIONS
    #############################################################

    def find_config(self, **kwargs):
        return self.db.session.query(Config).filter_by(**kwargs).first()

    def find_configs(self, order_by=None, **kwargs):
        if order_by:
            return self.db.session.query(Config).filter_by(**kwargs).order_by(order_by).all()
        return self.db.session.query(Config).filter_by(**kwargs).order_by('id').all()

    #####################################################################
    # PAGINATION FUNCTIONS
    #####################################################################

    def listify_filters(self, filters):
        filter_list = []
        for this_filter in filters.split(','):
            filter_list.append(this_filter)
        return filter_list

    def find_filter_config(self, endpoint):
        endpoint = endpoint.split('.')[1]
        pagination_config = ds_config.PAGINATION_CONFIG[endpoint]
        return pagination_config['filters']

    def paginate_items(self, page, q, endpoint, sort, direction):
        pagination_config = ds_config.PAGINATION_CONFIG[endpoint]

        model = eval(pagination_config['model'])
        per_page = pagination_config['per_page']

        order_values = '{0} {1}'.format(sort, direction)

        query = self.db.session.query(model)

        search_query = '%{0}%'.format(q)
        search_chain = []

        if endpoint in ['users', 'admin_users']:
            if current_app.config['REQUIRE_USERNAME']:
                search_chain = [model.email.ilike(search_query),
                                model.username.ilike(search_query),
                                model.first_name.ilike(search_query),
                                model.last_name.ilike(search_query),
                                model.id.ilike(search_query)]
            else:
                search_chain = [model.email.ilike(search_query),
                                model.first_name.ilike(search_query),
                                model.last_name.ilike(search_query),
                                model.id.ilike(search_query)]
        elif endpoint in ['roles', 'admin_roles']:
            search_chain = [model.name.ilike(search_query),
                            model.description.ilike(search_query)]
        elif endpoint == 'blog':
            search_chain = [model.title.ilike(search_query),
                            model.body.ilike(search_query)]
        elif endpoint == 'messages':
            from flask_security import current_user
            import re
            # Check to see if we're passing an id of a specific message in
            pattern = re.compile(pattern='id(\d+)')
            user_message_ids = re.findall(pattern, search_query)
            # Validate that only non deleted messages for the current user display
            query = query.join(Message).filter(model.user_id == current_user.id).filter(model.deleted_date == None)
            if len(user_message_ids) > 0:
                query = query.filter(model.id == user_message_ids[0])
            else:
                # Apply searches to our joined Message table
                query = query.filter(or_(Message.subject.like(search_query), Message.body.like(search_query)))
                # Apply order to our joined MessageType table
                if 'type' in order_values:
                    query = query.order_by(Message.type_id)
                    return query.paginate(page, per_page, False)

        if len(search_chain) > 0:
            query = query.filter(or_(*search_chain))
        query = query.order_by(text(order_values))

        return query.paginate(page, per_page, False)

    '''
    #####################################################################
    # /BASE FUNCTIONS
    #####################################################################
    '''

    '''
    #####################################################################
    # CONTENT FUNCTIONS
    #####################################################################
    '''

    def find_content(self, draft=False, name=None, page=None, **kwargs):
        if name and page:
            kwargs.update({'type': self.find_content_type(name=name, page=page)})
        if draft:
            return self.db.session.query(ContentDraft).filter_by(**kwargs).first()
        this = self.db.session.query(Content).filter_by(**kwargs).first()
        return this

    def find_contents(self, draft=False, order_by=None, **kwargs):
        if order_by is None:
            order_by = 'id desc'
        if draft:
            return self.db.session.query(ContentDraft).filter_by(**kwargs).order_by(order_by).all()
        return self.db.session.query(Content).filter_by(**kwargs).order_by(order_by).all()

    def find_content_type(self, **kwargs):
        return self.db.session.query(ContentType).filter_by(**kwargs).first()

    def modify_content_draft(self, content, **kwargs):
        if kwargs.get('description'):
            content.description = kwargs['description']
        if kwargs.get('text'):
            content.text = kwargs['text']
        self.put(content)
        return content

    def publish_content(self, content, content_draft):
        content.text = content_draft.text
        self.put(content)
        return content

    '''
    #####################################################################
    # CONTENT FUNCTIONS
    #####################################################################
    '''

    #############################################################
    # LEGAL FUNCTIONS
    #############################################################

    def find_legal_doc(self, **kwargs):
        return self.db.session.query(LegalDoc).filter_by(**kwargs).first()

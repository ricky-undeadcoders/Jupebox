$(document).ready(function () {
    //-- display active nav --//
    var active_nav = $('a[href="' + this.location.pathname + '"]');
    active_nav.parents('li,ul').addClass('active');
    if (active_nav.hasClass('dropdown-item')) {
        active_nav.addClass('active');
    }
    //-- /display active nav --//
    navScroll();
    $(window).bind('scroll', function (e) {
        navScroll();
    });

    // function navScroll() {
    //     var navbar = $('.navbar');
    //     var navbar_brand = $('.navbar-brand');
    //     var navbar_collapse = $('.navbar-collapse');
    //     var scrolled = $(window).scrollTop();
    //     if (scrolled > 21) {
    //         navbar.css('height', '90px');
    //         // navbar.css('border-bottom', '1px solid lightgrey');
    //         navbar_brand.css('font-size', '28px');
    //         navbar_brand.css('padding-top', '0');
    //         navbar_brand.css('padding-bottom', '0');
    //         navbar_collapse.css('font-size', '.9rem');
    //     }
    //     else {
    //         navbar.css('height', '');
    //         // navbar.css('border-bottom', '');
    //         navbar_brand.css('font-size', '');
    //         navbar_brand.css('padding-top', '');
    //         navbar_brand.css('padding-bottom', '');
    //         navbar_collapse.css('font-size', '');
    //     }
    // }
});

//-- navbar --//
$('ul.nav a.dropdown-toggle').click(function () {
    if ($(this).parent().hasClass('show')) {
        location.href = $(this).attr('href');
    }
});
//-- /navbar --//


__name__ = 'Jupebox'
__version__ = '1.0.1'

__release_notes__ = '''
1.0.1:
    Contact form working
    Lowered image resolutions
1.0.0:
    First Release
'''
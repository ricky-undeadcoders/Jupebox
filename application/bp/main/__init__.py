#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import Blueprint
from os.path import join, dirname, realpath

from application.bp.main.views_html import (homepage,
                                            about,
                                            contact,
                                            faq)
from application.bp.main.views_json import (contact_us)

blueprint = Blueprint(name='main',
                      import_name=__name__,
                      template_folder=join(dirname(realpath(__file__)), 'templates'))
blueprint.__version__ = '1.0.0'

##############################################
# HTML RULES
##############################################
blueprint.add_url_rule('/', 'homepage', homepage, methods=['GET'])
# blueprint.add_url_rule('/about/', 'about', about, methods=['GET'])
# blueprint.add_url_rule('/contact/', 'contact', contact, methods=['GET'])
# blueprint.add_url_rule('/faq/', 'faq', faq, methods=['GET'])

##############################################
# JSON RULES
##############################################
blueprint.add_url_rule('/contact-us/', 'contact_us', contact_us, methods=['POST'])

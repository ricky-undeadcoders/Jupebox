#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import (render_template, g, current_app, request, abort)

from application.bp.main.forms import ContactUsForm


def decide_draft():
    request_vars = filter(lambda a: a != '', str(request.url_rule).split('/'))
    if 'draft' in request_vars:
        return True
    return False


def homepage():
    return render_template('main/home_page.html',
                           contact_us_form=ContactUsForm(),
                           draft=decide_draft())


def about():
    return render_template('main/about.html',
                           draft=decide_draft())


def contact():
    return render_template('main/contact.html',
                           contact_us_form=ContactUsForm(),
                           draft=decide_draft())


def faq():
    return render_template('main/faq.html',
                           draft=decide_draft())

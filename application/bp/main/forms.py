#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import g, current_app, render_template
from flask_mail import Message
from flask_wtf import RecaptchaField
from wtforms import (StringField)
from wtforms.validators import Email, DataRequired, URL


from application.forms import CustomForm


class ContactUsForm(CustomForm):
    email = StringField(label='What is your email?', validators=[DataRequired(), Email()])
    name = StringField(label='What is your name?', validators=[DataRequired()])
    message = StringField(label='How can I help you?', validators=[DataRequired()])
    captcha = RecaptchaField()

    def __init__(self, *args, **kwargs):
        CustomForm.__init__(self, **kwargs)
        self.type = 'put'
        self.success_message = 'Thank you for your interest. We will get back to you soon!'

    def validate_on_submit(self):
        if not CustomForm.validate_on_submit(self):
            return self.validation_error()
        self.kwargs.update({'email': self.email.data})
        self.kwargs.update({'message': self.message.data})
        self.kwargs.update({'name': self.name.data})
        return self.validation_success()

    def update_data(self):
        if not self.validated:
            self.update_error()
        current_app.logger.info('WE GOT ONNNNNNNNEEEEEEEEEEEEEEEE!')
        current_app.logger.info('********************RIIIIIIIINNNNNNNNNGGGGGGGGGGG**********************')

        subject = 'We got one!'
        recipients = ['ricky.whitaker@undeadcodersociety.com', 'peej128@gmail.com', 'jupewhit@gmail.com']

        current_app.logger.info('Sending email "{}" to\n{}'.format(subject, recipients))
        mail = current_app.extensions.get('mail')
        message = Message(subject=subject,
                          sender=current_app.config['MAIL_DEFAULT_SENDER'],
                          recipients=recipients)

        message.html = render_template('email/we_got_one.html', form=self)
        current_app.logger.info(f'Our email template: {message.html}')

        if current_app.testing is False:
            mail.send(message)
            current_app.logger.info('Email sent!')

        current_app.logger.info('Successfully emailed contact')
        return self.update_success()

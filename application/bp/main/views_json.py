#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import (render_template, g, current_app, request, abort, jsonify, get_flashed_messages, flash)

from application.bp.main.forms import ContactUsForm


def contact_us():
    try:
        form = ContactUsForm()
        if form.validate_on_submit():
            if form.update_data():
                return jsonify({'message': get_flashed_messages(category_filter='success')}), 200, {}
    except Exception as e:
        current_app.logger.error('Fatal error attempting to update contact_us; error: {}'.format(e))
        flash(current_app.config['GENERIC_FORM_ERROR_MESSAGE'], 'error')

    return jsonify({'message': get_flashed_messages(category_filter='error')}), 500, {}


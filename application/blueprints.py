#!/usr/bin/python3
# -*- coding: utf-8 -*-
from application.bp.main import blueprint as main_bp

blueprints = [
    main_bp
]
